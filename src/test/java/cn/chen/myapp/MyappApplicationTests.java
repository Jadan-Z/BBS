package cn.chen.myapp;

import cn.chen.myapp.dao.UserDaoImpl;
import cn.chen.myapp.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RunWith(SpringRunner.class)
@SpringBootTest
@RestController
public class MyappApplicationTests {

    @Autowired
    UserDaoImpl userDao;

    @Test
    public void contextLoads() {
    }

    @Test
    @PostMapping(value = "/testData")
    public void data(){
        System.out.println(userDao.findUser("admin"));
    }
}
