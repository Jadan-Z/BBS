package cn.chen.myapp.test;

import cn.chen.myapp.dao.ArticleDaoImpl;
import cn.chen.myapp.dao.UserDaoImpl;
import cn.chen.myapp.entity.Article;
import cn.chen.myapp.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class testRest {
    @Autowired
    UserDaoImpl userDao;

    @Autowired
    ArticleDaoImpl articleDao;

    @PostMapping(value = "/testRest")
    public User data() {
        return userDao.findUser("admin");
    }

    @PostMapping(value = "/data1")
    public List<Article> getList(){
        return articleDao.findAll();
    }
}
