package cn.chen.myapp.mapper;

import cn.chen.myapp.entity.Comments;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CommentsMapper {

    @Select("select * from comments where articleID = #{articleID}")
    List<Comments> commentsList(int articleID);

    @Insert("insert into comments(articleID, userName, commentsText, time) values (#{articleID}, #{userName}, #{commentsText}, #{time})")
    void addComments(@Param("articleID") int articleID, @Param("userName") String userName, @Param("commentsText") String commentsText, @Param("time") String time);

}
