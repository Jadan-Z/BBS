package cn.chen.myapp.mapper;

import cn.chen.myapp.entity.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {

    @Select("select * from user")
    List<User> findAll();

    @Select("SELECT * FROM USER WHERE userAccount = #{userAccount}")
    User Select(String userAccount);

    @Insert("INSERT INTO USER(userAccount,userPassword) VALUES (#{userAccount},#{userPassword})")
    int Insert(@Param("userAccount") String userAccount, @Param("userPassword") String userPassword);

    @Update("UPDATE USER SET userPassword = #{newPassword} WHERE userPassword = #{oldPassword}")
    int Updata(@Param("oldPassword") String oldPassword, @Param("newPassword") String newPassword);


}
