package cn.chen.myapp.mapper;

import cn.chen.myapp.entity.Article;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ArticleMapper {

    @Select("select * from article")
    List<Article> findAll();

    @Select("select * from article where articleTitle = #{articleTitle}")
    List<Article> findByTitle(String articleTitle);

    @Select("select * from article where articleTitle like #{keyWord}")
    List<Article> findByLikeTitle(String keyWord);

    @Insert("insert into article (articleTitle, articleText, userName, time) values (#{articleTitle},#{articleText},#{userName},#{time})")
    void addArticle(@Param("articleTitle") String articleTitle, @Param("articleText") String articleText, @Param("userName") String userName, @Param("time") String time);

    @Select("select * from article where userName = #{userAccount}")
    List<Article> myArticle(String userAccount);

    @Select("select * from article where articleID = #{articleID}")
    Article getByID(int articleID);

}
