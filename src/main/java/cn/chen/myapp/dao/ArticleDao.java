package cn.chen.myapp.dao;

import cn.chen.myapp.entity.Article;

import java.util.List;

public interface ArticleDao {

    //获取所有文章
    List<Article> findAll();

    //根据文章标题查询
    List<Article> findByTitle(String articleTitle);

    //标题模糊查询
    List<Article> findByLikeTitle(String articleTitle);

    //添加文章
    void addArticle(String articleTitle, String articleText, String userName, String time);

    //用户的文章
    List<Article> myArticle(String userAccount);

    //根据ID查找对应的文章
    Article getByID(int articleID);
}
