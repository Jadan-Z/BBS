package cn.chen.myapp.dao;

import cn.chen.myapp.entity.Article;
import cn.chen.myapp.mapper.ArticleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ArticleDaoImpl implements ArticleDao {

    @Autowired
    ArticleMapper mapper;

    @Override
    public List<Article> findAll() {
        return mapper.findAll();
    }

    @Override
    public List<Article> findByTitle(String articleTitle) {
        return mapper.findByTitle(articleTitle);
    }

    @Override
    public List<Article> findByLikeTitle(String articleTitle) {
        return mapper.findByLikeTitle(articleTitle);
    }

    @Override
    public void addArticle(String articleTitle, String articleText, String userName, String time) {
        mapper.addArticle(articleTitle, articleText, userName, time);
    }

    @Override
    public List<Article> myArticle(String userAccount) {
        return mapper.myArticle(userAccount);
    }

    @Override
    public Article getByID(int articleID) {
        return mapper.getByID(articleID);
    }
}


