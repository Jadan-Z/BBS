package cn.chen.myapp.dao;

import cn.chen.myapp.entity.User;

import java.util.List;

public interface UserDao {

    //查询用户列表
    List<User> findAll();

    //根据用户账户查找用户
    User findUser(String userAccount);

    //添加用户
    int addUser(String userAccount, String userPassword);

    //修改用户密码
    int updateUser(String oldPass, String newPass);
}
