package cn.chen.myapp.dao;

import cn.chen.myapp.entity.Comments;
import cn.chen.myapp.mapper.CommentsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class CommentsDaoImpl implements CommentsDao {

    @Autowired
    CommentsMapper mapper;

    @Override
    public List<Comments> commentsList(int articleID) {
        return mapper.commentsList(articleID);
    }

    @Override
    public void addComments(int articleID, String userName, String commentsText, String time) {
        mapper.addComments(articleID, userName, commentsText, time);
    }
}
