package cn.chen.myapp.dao;

import cn.chen.myapp.entity.User;
import cn.chen.myapp.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class UserDaoImpl implements UserDao {

    @Autowired
    UserMapper mapper;

    @Override
    public List<User> findAll(){
        return mapper.findAll();
    }

    @Override
    public User findUser(String userAccount) {
        return mapper.Select(userAccount);
    }

    @Override
    public int addUser(String userAccount, String userPassword) {
        return mapper.Insert(userAccount, userPassword);
    }

    @Override
    public int updateUser(String oldPass, String newPass) {
        return mapper.Updata(oldPass, newPass);
    }
}
