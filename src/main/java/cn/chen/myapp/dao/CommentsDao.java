package cn.chen.myapp.dao;

import cn.chen.myapp.entity.Comments;

import java.util.List;

public interface CommentsDao {

    //根据文章ID查询评论
    List<Comments> commentsList(int articleID);

    //（发表）添加评论
    void addComments(int articleID, String userName, String commentsText, String time);
}
