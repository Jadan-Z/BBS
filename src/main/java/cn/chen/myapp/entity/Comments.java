package cn.chen.myapp.entity;

import org.springframework.stereotype.Controller;

import javax.validation.constraints.Null;

@Controller
public class Comments {
    @Null(message = "数据库自动生成！")
    private int commentsID;
    private int articleID;
    private String userName;
    private String commentsText;
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getCommentsID() {
        return commentsID;
    }

    public void setCommentsID(int commentsID) {
        this.commentsID = commentsID;
    }

    public int getArticleID() {
        return articleID;
    }

    public void setArticleID(int articleID) {
        this.articleID = articleID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCommentsText() {
        return commentsText;
    }

    public void setCommentsText(String commentsText) {
        this.commentsText = commentsText;
    }
}
