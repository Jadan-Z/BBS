package cn.chen.myapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.jws.WebParam;

@Controller
@RequestMapping(value = "/userList")
public class testController {

    @RequestMapping(value = "/data")
    public String userList(Model model) {
        model.addAttribute("dataInfo","数据");
        return "userList";
    }

    @RequestMapping(value = "/change")
    public String change(Model model, @ModelAttribute("data") String data) {
        model.addAttribute("dataInfo",data);
        return "userList";
    }
}

