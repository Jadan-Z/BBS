package cn.chen.myapp.controller;

import cn.chen.myapp.dao.ArticleDaoImpl;
import cn.chen.myapp.entity.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
public class ArticleController {

    @Autowired
    List<Article> articleList;

    @Autowired
    ArticleDaoImpl articleDao;

    @RequestMapping(value = "/manage/search")
    public String search(@ModelAttribute("title") String title, Model model, HttpSession session) {
        String tmp = "%" + title + "%";
        articleList = articleDao.findByLikeTitle(tmp);
        model.addAttribute(articleList);
        model.addAttribute("location", "所有文章>搜索结果");
        model.addAttribute("userAccount", session.getAttribute("userAccount"));
        System.out.println("xxx.");
        return "manage";
    }
}
