package cn.chen.myapp.controller;

import cn.chen.myapp.dao.ArticleDao;
import cn.chen.myapp.dao.ArticleDaoImpl;
import cn.chen.myapp.entity.Article;
import cn.chen.myapp.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class ManageController {

    @Autowired
    ArticleDaoImpl articleDao;

    @Autowired
    User user;

    @Autowired
    Article article;

    //获取时间戳
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
    String date = df.format(new Date());

    //所有文章
    @RequestMapping(value = "/manage/all")
    public String allArticle(Model model, HttpSession session) {
        List<Article> articleList = articleDao.findAll();
        Collections.reverse(articleList);

        model.addAttribute("location", "所有文章");
        model.addAttribute("userAccount", session.getAttribute("userAccount"));
        model.addAttribute(articleList);
        return "manage";
    }

    //用户文章
    @RequestMapping(value = "/manage/my-article")
    public String myArticle(Model model, HttpSession session) {
        List<Article> myArticle = articleDao.myArticle(session.getAttribute("userAccount").toString());
        Collections.reverse(myArticle);
        model.addAttribute("location", "我的文章");
        model.addAttribute("userAccount", session.getAttribute("userAccount"));
        model.addAttribute(myArticle);
        return "manage";
    }

    //写文章
    @RequestMapping(value = "/manage/write-article")
    public String writeArticle(@Param("title") String title,
                               @Param("text") String text,
                               Model model, HttpSession session) {

        model.addAttribute("userAccount", session.getAttribute("userAccount"));
        model.addAttribute("location", "所有文章");
        articleDao.addArticle(title, text, session.getAttribute("userAccount").toString(), date);

        List<Article> articleList = articleDao.findAll();
        model.addAttribute(articleList);
        return "manage";
    }
}
