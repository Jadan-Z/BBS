package cn.chen.myapp.controller;

import cn.chen.myapp.dao.UserDaoImpl;
import cn.chen.myapp.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RegisteredController {

    @Autowired
    UserDaoImpl userDao;

    @RequestMapping(value = "/registered")
    public String registered(Model model) {
        return "registered";
    }

    @RequestMapping(value = "/registered/check")
    public String registeredCheck(@ModelAttribute("userInfo") User user, Model model, BindingResult result) {
        userDao.addUser(user.getUserAccount(), user.getUserPassword());

        if (result.hasErrors()) {
            model.addAttribute("accountErr", "用户名已存在！");
            model.addAttribute("userAccount", "");
            model.addAttribute("password", "");
            model.addAttribute("userPassword", "");
            return "registered";
        }else{
            model.addAttribute("userAccount", user.getUserAccount());
            model.addAttribute("password", "");
            model.addAttribute("userPassword", "");
            return "registered";
        }
    }
}
