package cn.chen.myapp.controller;

import cn.chen.myapp.dao.ArticleDaoImpl;
import cn.chen.myapp.dao.CommentsDaoImpl;
import cn.chen.myapp.entity.Article;
import cn.chen.myapp.entity.Comments;
import cn.chen.myapp.mapper.CommentsMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class CommentsController {

    @Autowired
    List<Comments> commentsList;

    @Autowired
    Article article;

    @Autowired
    CommentsDaoImpl commentsDao;

    @Autowired
    ArticleDaoImpl articleDao;

    private int articleId;

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    //查看对应文章的评论
    @RequestMapping(value = "/manage/comments")
    public String tmp(Model model, HttpSession session, @RequestParam("testID") String testID){

        this.setArticleId(Integer.parseInt(testID));

        commentsList = commentsDao.commentsList(Integer.parseInt(testID));
        article = articleDao.getByID(Integer.parseInt(testID));

        model.addAttribute(commentsList);
        model.addAttribute("location", "查看评论>"+article.getArticleTitle());
        model.addAttribute("userAccount", session.getAttribute("userAccount"));
        return "comments";
    }

    //获取时间戳
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
    String date = df.format(new Date());

    //发表评论
    @RequestMapping(value = "/manage/comments/add")
    public String addComments(Model model,
                              @Param("text") String text,
                              HttpSession session) {
        int Id = this.getArticleId();
        commentsDao.addComments(Id, session.getAttribute("userAccount").toString(), text, date);
        commentsList = commentsDao.commentsList(Id);
        article = articleDao.getByID(Id);

        model.addAttribute(commentsList);
        model.addAttribute("location", "查看评论>"+article.getArticleTitle());
        model.addAttribute("userAccount", session.getAttribute("userAccount"));
        return "comments";
    }
}
