package cn.chen.myapp.controller;

import cn.chen.myapp.dao.ArticleDaoImpl;
import cn.chen.myapp.dao.UserDaoImpl;
import cn.chen.myapp.entity.Article;
import cn.chen.myapp.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class LoginController {

    @Autowired
    User user;

    @Autowired
    UserDaoImpl userDao;

    @Autowired
    ArticleDaoImpl articleDao;

    //定位首页为login.html
    @RequestMapping(value = "/")
    public String index(Model model){
        model.addAttribute("accountErr", "");
        model.addAttribute("passwordErr", "");
        return "login";
    }

    //@RequestMapping(value = "/check/accountErr")
    public String accountErr(Model model) {
        model.addAttribute("accountErr", "账号不存在！");
        model.addAttribute("passwordErr", "");
        return null;
    }

    //@RequestMapping(value = "/check/passwordErr")
    public String passwordErr(Model model) {
        model.addAttribute("accountErr", "");
        model.addAttribute("passwordErr", "密码错误！");
        return null;
    }

    //用户验证
    @RequestMapping(value = "/manage")
    public String loginCheck(Model model,@ModelAttribute("user") User userForm,
                             BindingResult result, HttpSession session) {

        List<Article> articleList = articleDao.findAll();
        user = userDao.findUser(userForm.getUserAccount());
        Collections.reverse(articleList);

        session.setAttribute("userAccount",userForm.getUserAccount());
        session.setAttribute("userPassword",userForm.getUserPassword());
        session.setMaxInactiveInterval(9000);

        if (user.getUserPassword().equals(userForm.getUserPassword())) {
            Map<String, Object> userMap = new HashMap<>();

            userMap.put("userAccount",user.getUserAccount());
            userMap.put("location", "所有文章");
            model.addAttribute(articleList);
            model.addAllAttributes(userMap);
            model.addAttribute("userAccount", userMap.get("userAccount"));
            return "manage";
        } else {
            if (userForm.getUserAccount() == null) {

                return "login";
            } else {
                return "login";
            }
        }
    }

}